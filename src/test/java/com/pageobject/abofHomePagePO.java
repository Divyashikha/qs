package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class abofHomePagePO {
	
AndroidDriver driver=null;
	
	public abofHomePagePO(AndroidDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(id="com.abof.android:id/landing_okay_gt_it_btn")
	private WebElement eleokPopupBtn;
	public WebElement getEleOkPopupBtn(){
		return eleokPopupBtn;
	}
	
	@FindBy(name="MEN")
	private WebElement eleMenTab;
	public WebElement getEleMenTab(){
		return eleMenTab;
	}
	
	@FindBy(xpath="//android.widget.RelativeLayout[@index='2']//android.widget.FrameLayout[@index='0']")
	private WebElement eleClothingTab;
	public WebElement getEleClothingTab(){
		return eleClothingTab;
		
	}
	
	@FindBy(name="Casual Shirts")
	private WebElement eleCasualShirtsText;
	public WebElement getEleCasualShirtsText(){
		return eleCasualShirtsText;
		
	}
	
	@FindBy(xpath="//android.widget.TextView[contains(@text,'Men')]")
	private WebElement eleProductnameText;
	public WebElement getEleProductnameText(){
		return eleProductnameText;
		
	}
	
	@FindBy(id="com.abof.android:id/prduct_name")
	private List<WebElement> eleProduct;
	public List<WebElement> getEleProduct(){
		return eleProduct;
	}
	
	@FindBy(id="com.abof.android:id/imgview")
	private WebElement eleImageView;
	public WebElement getEleImageView(){
		return eleImageView;
		
	}
	
	@FindBy(id="com.abof.android:id/closeview")
	private WebElement eleCloseView;
	public WebElement getEleCloseView(){
		return eleCloseView;
	}

}
