package com.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class abofLoginPo {
	AndroidDriver driver=null;
	
	public abofLoginPo(AndroidDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}
	@FindBy(id="com.abof.android:id/login_email_edt")
		private WebElement eleUsernameTextBox;
	public WebElement getEleUserNameTextBox(){
		return eleUsernameTextBox;
	}
	
	@FindBy(id="com.abof.android:id/login_password_edt_edt")
	private WebElement elepasswordtextBox;
	public WebElement getElepasswordtextbox(){
		return elepasswordtextBox;
	}
	
	@FindBy(name="Sign in")
	private WebElement eleSignBtn;
	public WebElement getEleSignBtn(){
		return eleSignBtn;
	}
}
	



