package com.BaseLib;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Dimension;

public class genericClass {
	
	public static String sconfig=BaseClass.sDirpath+"\\conig.properties";
	public static String sexcel=BaseClass.sDirpath+"\\Abof.xlsx";
	public static Dimension dSize;
	//read the data from congigProperties
	public static String readconigproperties(String skey){
		String svalue=null;
		Properties properties=new Properties();
		try{
			FileInputStream fis=new FileInputStream(sconfig);
			properties.load(fis);
			svalue=properties.getProperty(skey);
			
		}catch(Exception e){
			
		}
		return svalue;
	}
	
	//read data from the excel sheet
	public static String[] readdata(String sheet,String TestcasesID )
	{
		String sData[]=null;
		try{
			FileInputStream fis=new FileInputStream(sexcel);
			Workbook wb=(Workbook)WorkbookFactory.create(fis);
			Sheet sh=wb.getSheet(sheet);
			int srow=sh.getLastRowNum();
			System.out.println(srow);
			for(int i=1;i<=srow;i++){
				if(sh.getRow(i).getCell(0).toString().equals(TestcasesID)){
					int scell=sh.getRow(i).getLastCellNum();
					System.out.println(scell);
					sData=new String[scell];
					for(int j=0;j<scell;j++){
						sData[j]=sh.getRow(i).getCell(j).getStringCellValue();
					}
					break;
					
				}
			}
	}catch(Exception e){
		
	}
		return sData;
	}
	
	//Scroll the page
	public static void scroll(double starty,double endy) throws Exception{
		try{
			
				dSize=BaseClass.driver.manage().window().getSize();
				System.out.println(dSize);
				int STARTY=(int)(dSize.height*starty);
				System.out.println(STARTY);
				int ENDY=(int)(dSize.height*endy);
				System.out.println(ENDY);
				int startx=dSize.width/2;
				System.out.println(startx);
				BaseClass.driver.swipe(startx, STARTY, startx, ENDY, 2000);
			
		}catch(Exception e){
			throw e;
		}
	}
		//swipe the image
		
		public static void swipe(double startx,double endx){
			try{
				dSize=BaseClass.driver.manage().window().getSize();
				int STARTX=(int)(dSize.width*startx);
				int ENDX=(int)(dSize.width*endx);
				int starty=dSize.height/2;
				BaseClass.driver.swipe(STARTX, starty, ENDX, starty,2000);
				
			}catch(Exception e){
				
			}
		}
}

