package com.BaseLib;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.kirwa.nxgreport.NXGReports;

import bsh.Capabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class BaseClass {
	
	public DesiredCapabilities Capabilities;
	public static String sDirpath=System.getProperty("user.dir");
	public static String apkpath=sDirpath+"\\abof.apk";
	public static AndroidDriver driver;
	public static String nodejs="C:\\Program Files (x86)\\Appiumlatest\\node.exe";
	public static String appiumjs="C:\\Program Files (x86)\\Appiumlatest\\node_modules\\appium\\bin\\appium.js";
	public static AppiumDriverLocalService service;
	
	
	@BeforeSuite
	public void startserver(){
		try{
				service=AppiumDriverLocalService.buildService(new AppiumServiceBuilder().usingPort(4723).usingDriverExecutable(new File(nodejs)).withAppiumJS(new File(appiumjs)));
				service.start();
		}catch(Exception e)
		{
			
		}
	}
	@BeforeTest
	public void setup() throws MalformedURLException{
		File sfile=new File(apkpath);
		Capabilities=new DesiredCapabilities();
		Capabilities.setCapability("automationName",genericClass.readconigproperties("AUTOMATIONNAME"));
		Capabilities.setCapability("platformName",genericClass.readconigproperties("PLATFORMNAME"));
		Capabilities.setCapability("deviceName",genericClass.readconigproperties("DEVICENAME"));
		Capabilities.setCapability("platformVersion",genericClass.readconigproperties("PLATFORMVERSION"));
		Capabilities.setCapability("app",sfile.getAbsolutePath());
		Capabilities.setCapability("appPackage", genericClass.readconigproperties("ABOFPAKACE"));	
		Capabilities.setCapability("appActivity", genericClass.readconigproperties("ABOFACTIVITY"));
		driver=new AndroidDriver(new URL(genericClass.readconigproperties("APPIUMSERVERURL")),Capabilities);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		NXGReports.setWebDriver(driver);
	}
	
	@AfterTest
	public void close(){
		driver.quit();
	}
	
	@AfterSuite
	public void stopserver(){
		service.stop();
	}
	
}
