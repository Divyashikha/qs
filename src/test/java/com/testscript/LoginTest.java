package com.testscript;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.BaseLib.BaseClass;
import com.BaseLib.genericClass;
import com.kirwa.nxgreport.NXGReports;
import com.kirwa.nxgreport.logging.LogAs;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen;
import com.kirwa.nxgreport.selenium.reports.CaptureScreen.ScreenshotOf;
import com.pageobject.abofHomePagePO;
import com.pageobject.abofLoginPo;

public class LoginTest extends BaseClass{
	String sData[]=null;
	abofLoginPo lognpo=null;
	abofHomePagePO homepo=null;
	//Login to the abof application and go the payment page
	@BeforeMethod
	public void object(){
		lognpo=new abofLoginPo(driver);
		homepo=new abofHomePagePO(driver);
	}
	
	@Test(priority=1,enabled=true,description="Login to the abof application")
	public void vetifylogin() throws Exception{
		
		sData=genericClass.readdata("Sheet1","AbofLogin_01");
		try{
			System.out.println(sData[1]);
			System.out.println(sData[2]);
			Assert.assertTrue(lognpo.getEleUserNameTextBox().isDisplayed(), "The username textbox is not displayed");
			NXGReports.addStep("The username textbox is displayed", LogAs.PASSED, null);
			lognpo.getEleUserNameTextBox().sendKeys(sData[1]);
			NXGReports.addStep("the email id is passed to the username testbox", LogAs.PASSED,null);
			driver.hideKeyboard();
			Assert.assertTrue(lognpo.getElepasswordtextbox().isDisplayed(),"The password textbox is not displayed");
			NXGReports.addStep("the password textbox is displayed", LogAs.PASSED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			lognpo.getElepasswordtextbox().sendKeys(sData[2]);
			driver.hideKeyboard();
			Assert.assertTrue(lognpo.getEleSignBtn().isDisplayed(),"The sign button is not displayed");
			lognpo.getEleSignBtn().click();
			
			Thread.sleep(4000);
			driver.findElement(By.id("com.abof.android:id/cancel_btn")).click();
			Thread.sleep(4000);
			Assert.assertTrue(homepo.getEleOkPopupBtn().isDisplayed(),"The ok popup is not displayed");
			NXGReports.addStep("The ok popup is displayed", LogAs.PASSED,null);
			homepo.getEleOkPopupBtn().click();
			homepo.getEleMenTab().click();
			Assert.assertTrue(homepo.getEleClothingTab().isDisplayed(),"the clothingTab is not displayed");
			NXGReports.addStep("the clothing tab is displayed", LogAs.PASSED,null);
			
			homepo.getEleClothingTab().click();
			homepo.getEleCasualShirtsText().click();
			
			/*System.out.println(homepo.getEleProductnameText().getText());
			Assert.assertTrue(homepo.getEleProductnameText().isDisplayed(), "The product name is not displayed");
			NXGReports.addStep("the productname is displayed", LogAs.PASSED, null);*/
			Thread.sleep(4000);
			homepo.getEleProduct().get(0).click();
			Thread.sleep(4000);
			homepo.getEleImageView().click();
			Thread.sleep(4000);
			for(int i=1;i<=5;i++){
				Thread.sleep(4000);
			genericClass.swipe(.90,.10);
			}
			homepo.getEleCloseView().click();
			homepo.getEleImageView().click();
			Thread.sleep(4000);
			driver.zoom(driver.findElement(By.id("com.abof.android:id/zoomview")));
			Thread.sleep(4000);
			/*Thread.sleep(4000);
			driver.navigate().back();
			homepo.getEleProduct().get(1).click();
			Thread.sleep(4000);
			driver.navigate().back();
			Thread.sleep(4000);
			genericClass.scroll(.90,.20);
			Thread.sleep(4000);
			homepo.getEleProduct().get(2).click();*/
			Thread.sleep(6000);
		}catch(Exception e){
			throw e;
			
			
		}
	}
	

}
